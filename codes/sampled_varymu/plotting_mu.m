close all;

f1 = figure;
set(gca,'fontsize',25);
%grid;
hold on;
box;
eb1 = errorbar(1:0.25:2.5,mean(err_d_SSHOPM,2),std(err_d_SSHOPM,0,2),'b','LineWidth',3,'Marker', 'o');
eb2 = errorbar(1:0.25:2.5,mean(err_d_SSHOPM0,2),std(err_d_SSHOPM0,0,2),'r','LineWidth',3,'Marker', 'o');
%eb2 = errorbar(400:100:800,mean(err_r(:,2,:),3),std(err_r(:,2,:),0,3),'r','LineWidth',3,'Marker', 'o');
%eb3 = errorbar(400:100:800,mean(err_r(:,3,:),3),std(err_r(:,3,:),0,3),'m','LineWidth',3);
set(get(eb1,'Parent'), 'YScale', 'log');
set(get(eb2,'Parent'), 'YScale', 'log');
%set(get(eb3,'Parent'), 'YScale', 'log');
axis tight;
legend('Whiten','Nonwhiten','location','SouthEast');
legend('boxoff');
xlabel('\mu');
ylabel('Error');
%ylabel('Time');
title('n = 100, d = 60, r = 5');

saveTightFigure(f1, 'ten_err_r_2');
%%
f2 = figure;
set(gca,'fontsize',25);
%grid;
hold;
box;
eb1 = errorbar(1:0.25:2.5,mean(time_d_SSHOPM,2),std(time_d_SSHOPM,0,2),'b','LineWidth',3,'Marker', 'o');
eb2 = errorbar(1:0.25:2.5,mean(time_d_SSHOPM0,2),std(time_d_SSHOPM0,0,2),'r','LineWidth',3,'Marker', 'o');
%eb3 = errorbar(400:100:800,mean(time_r(:,3,:),3),std(time_r(:,3,:),0,3),'m','LineWidth',3);
set(get(eb1,'Parent'), 'YScale', 'log');
%set(get(eb2,'Parent'), 'YScale', 'log');
%set(get(eb3,'Parent'), 'YScale', 'log');
axis tight;
legend('Whiten','Nonwhiten','location','SouthEast');
legend('boxoff');
xlabel('\mu');
ylabel('Time(s)');
title('n = 100, d = 60, r = 5');

saveTightFigure(f2, 'ten_time_r_2');

% f3 = figure;
% set(gca,'fontsize',25);
% %grid;
% hold;
% box;
% p = plot(rank_iters,'b','LineWidth',3,'Marker', 'o');
% set(get(p,'Parent'), 'YScale', 'log');
% axis tight;
% legend('IALM','location','NorthWest');
% legend('boxoff');
% xlabel('Iterations');
% ylabel('Rank');
% title('n = 2000, r = 5, \mu = 1');
% 
% saveTightFigure(f3, 'rank_r_2');
% 
% f4 = figure;
% set(gca,'fontsize',25);
% %grid;
% hold;
% box;
% mr = errorbar(400:100:800,mean(max_rank_r,2),std(max_rank_r,0,2),'b','LineWidth',3,'Marker','o');
% %p = plot(mean(max_rank_,'b','LineWidth',3,'Marker', 'o');
% %set(get(mr,'Parent'), 'YScale', 'log');
% axis tight;
% legend('IALM','location','NorthWest');
% legend('boxoff');
% xlabel('n \alpha');
% ylabel('Max. Rank');
% title('n = 2000, r = 5, \mu = 1');
% 
% saveTightFigure(f4, 'max_rank_r_2');
