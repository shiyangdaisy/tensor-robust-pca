clear;
for avg_runs = 1:5
    i_d = 1
    for incoh = 1:0.25:2.5
        clearvars -except avg_runs i_d incoh time_d_SSHOPM0 err_d_SSHOPM0 time_d_SSHOPM err_d_SSHOPM ;
        clc;
        fprintf('mu= %d\n', incoh);
        n1 = 100;
        n2 = 100;
        n3 = 100;
        n = 100;
        r = 5;
        d = 60;
        n_init = r*5;
        randn_mat = zeros(n1,n2,n3);
%%%%%%%%%%%%%%%%%%%%% generate L %%%%%%%%%%%%%%%%%%%%%%%%%%
        y1 = zeros(n1,r);
        y2 = zeros(n2,r);
        y3 = zeros(n3,r);
        z_n1 = floor(n1/incoh^2);
        rp = randperm(n1);
        y1(rp(1:z_n1), :) = randn(z_n1, r);
        z_n2 = floor(n2/incoh^2);
        rp = randperm(n2);
        y2(rp(1:z_n2), :) = randn(z_n2, r);
        z_n3 = floor(n3/incoh^2);
        rp = randperm(n3);
        y3(rp(1:z_n3), :) = randn(z_n3, r);
        
        %%%%%%%%%%%% un orthogonalize %%%%%%%%%%%%%%%%%%%%
%         R = zeros(r,r);
%         rp = randperm(r);
%         for i = 1:r
%           R(rp(1:r),i) = rand(r,1);
%         end
%         
%         z1 = y1*R;
%   
          y1 = y1./repmat(sqrt(sum(y1.*y1,1)),n1,1);
%         %y2 = y2./repmat(sqrt(sum(y2.*y2,1)),n2,1);
%         %y3 = y3./repmat(sqrt(sum(y3.*y3,1)),n3,1);
%         z1 = z1+0.5.*ones(n,r);
%         z1 = z1./repmat(sqrt(sum(z1.*z1,1)),n1,1);
        %y1 = sortrows(y1',1)'; % make y1 increasing, easy to verify correctness
        y2 = y1;
        y3 = y1;
        %lam_vec = sort(0.9+(rand(r,1)/10),'descend');
        lam_vec = ones(r,1);
        condn_no = lam_vec(1)/lam_vec(end);
        L = ktensor(lam_vec,y1,y2,y3);

%%%%%%%%%%%%%%%%%% generate S %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
S = sptenrand([n1,n2,n3],d*n*n);
        s_nonzero_idx = find(S);
        S(s_nonzero_idx) = (r/sqrt(n1*n2*n3))*(rand(length(s_nonzero_idx),1)/2+.5).*sign(randn(length(s_nonzero_idx),1)); 
       
%%%%%%%%%%%%%%%%%%Whiten Matrix%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M2 = y1*y1';
[UO,SO,~] = svds(M2,r);
Whiten = UO*(sqrt(inv(SO)));

%%%%%%%%%%%%%%%% method one: tensor SSHOPM(not using whiten matrix) %%%%%%%%%%%%
fprintf('method one:tensor SSHOPM(not using whiten matrix)\t');
r_hat = r;
EPS = 1e-3;
EPS_S = 5e-3;
MAX_ITER = 51;
TOL = 1e-1;

tic;
[L_t0,S_t0,iter_am0,frob_err_am0] = ncrpca_ten_new(L,S,r,EPS,MAX_ITER,EPS_S,incoh,TOL);
time_am_SSHOPM0 = toc
%err_am_SSHOPM0 = norm(S-S_t0)/norm(S)
%err_am_SSHOPM0 = norm(L-L_t0)/norm(L)
ind = find(S_t0);
err_am_SSHOPM0 = norm(S(ind)-S_t0(ind))/norm(S(ind))
time_d_SSHOPM0(i_d,avg_runs) = time_am_SSHOPM0;
err_d_SSHOPM0(i_d,avg_runs) = err_am_SSHOPM0;

%%%%%%%%%%%%%%%% method two: tensor SSHOPM(using whiten matrix) %%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('method two:tensor SSHOPM(using whiten matrix)\t');
r_hat = r;
EPS = 1e-3;
EPS_S = 5e-3;
MAX_ITER = 51;
TOL = 1e-1;

tic;
[L_t,S_t,iter_am,frob_err_am] = ncrpca_ten_new_whiten(L,S,Whiten,r,EPS,MAX_ITER,EPS_S,incoh,TOL);
time_am_SSHOPM = toc
%err_am_SSHOPM = norm(S-S_t)/norm(S)
%err_am_SSHOPM = norm(L-L_t)/norm(L)
ind = find(S_t);
err_am_SSHOPM = norm(S(ind)-S_t(ind))/norm(S(ind))
time_d_SSHOPM(i_d,avg_runs) = time_am_SSHOPM;
err_d_SSHOPM(i_d,avg_runs) = err_am_SSHOPM;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% method three: matrix AltProj %%%%%%%%%%%%%%%%%%%%%%%%
% fprintf('method three: matrix AltProj\t');
% M_L = zeros(n1,n2,n3);
% M_S = zeros(n1,n2,n3);
% EPS = 1e-3;
% EPS_S = 5e-3;
% MAX_ITER = 51;
% lambda = 1/sqrt(n1);
% testtime = zeros(d,1);
% tic;
% for i = 1:1:n
%     M = double(T(:,:,i));
%     [ML_t, MS_t] = ncrpca(M, r, EPS, MAX_ITER, EPS_S, incoh);
%     M_L(:,:,i) = ML_t;
%     M_S(:,:,i) = MS_t;
% end
% time_ncrpca_AltProj = toc
% err_ncrpca_AltProj = norm(S-tensor(M_S))/norm(S)
% err_ncrpca_AltProj = norm(L-tensor(M_L))/norm(L)
% time_d_AltProj(i_d,avg_runs) = time_ncrpca_AltProj;
% err_d_AltProj(i_d,avg_runs) = err_ncrpca_AltProj;
%%%%%%%%%%%%%%%%%%%%%% Parameter recovery %%%%%%%%%%%%%%%%%%%%
% P = cp_als(L,r);
% PT0 = cp_als(L_t0,r);
% PT = cp_als(L_t,r);
% PM = cp_als(tensor(M_L),r);
% for i = 1:1:3
%     for j = 1:1:r
%         PT0.U{i}(:,j) = (PT0.lambda(j))^(1/3).*PT0.U{i}(:,j);       
%         PT.U{i}(:,j) = (PT.lambda(j))^(1/3).*PT.U{i}(:,j);
%         PM.U{i}(:,j) = (PM.lambda(j))^(1/3).*PM.U{i}(:,j);
%     end
% end
% 
% PU = findcorrectU(P,n,r);
% PTU0 = findcorrectU(PT0,n,r);
% PTU = findcorrectU(PT,n,r);
% PMU = findcorrectU(PM,n,r);
% 
% absPU = sortrows(PU',1)';
% absPTU0 = sortrows(PTU0',1)';
% absPTU = sortrows(PTU',1)';
% absPMU = sortrows(PMU',1)';
% err_L_SSHOPM0_MU1(i_d,avg_runs) = norm(absPU-absPTU0);
% err_L_SSHOPM_MU1(i_d,avg_runs) = norm(absPU-absPTU);
% err_L_AltProj_MU1(i_d,avg_runs) = norm(absPU-absPMU);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
i_d = i_d+1;
    end
end
save('time_d_SSHOPM0.mat','time_d_SSHOPM0');
save('err_d_SSHOPM0.mat','err_d_SSHOPM0');
save('time_d_SSHOPM.mat','time_d_SSHOPM');
save('err_d_SSHOPM.mat','err_d_SSHOPM');
% save('time_d_AltProj.mat','time_d_AltProj');
% save('err_d_AltProj.mat','err_d_AltProj');
% save('err_L_AltProj.mat','err_L_AltProj_MU1');
% save('err_L_SSHOPM.mat','err_L_SSHOPM_MU1');
% save('err_L_SSHOPM0.mat','err_L_SSHOPM0_MU1');
exit;