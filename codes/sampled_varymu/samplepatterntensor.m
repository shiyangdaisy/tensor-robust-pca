function SUBS = samplepatterntensor(n,r)
SUBS = randi(n,(n^2)*r,3);
end