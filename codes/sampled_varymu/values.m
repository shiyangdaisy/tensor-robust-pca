function Vals = values(SUBS,P,r)
I1 = SUBS(:,1);
I2 = SUBS(:,2);
I3 = SUBS(:,3);
lambda = P.lambda;
U1 = P.U{1};
U2 = P.U{2};
U3 = P.U{3};
L = repmat(lambda',size(I1,1),1);
Vals = sum(L.*U1(I1,:).*U2(I2,:).*U3(I3,:),2);


end