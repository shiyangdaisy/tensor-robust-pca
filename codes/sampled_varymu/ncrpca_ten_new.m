function [L_t,S_t,iters,frob_err] = ncrpca_ten_new(L,S,true_r,EPS,MAX_ITER,EPS_S,incoh,TOL)
frob_err(1) = inf;
n = size(L);
n1 = n(1);
n2 = n(2);
n3 = n(3);
t = 1;
idx = [];
thresh_const = 10; % threshold constant: can be tuned depending on incoherence
thresh_red = 0.9; % parameter to reduce the threshold constant: can be tuned
r_hat = 1; % initial rank for stagewise algorithm
L_t = sptensor([],[],[n1,n2,n3]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBS = samplepatterntensor(n1,n1^(1/2));
subsL = values(SUBS,L,true_r);
temp = intersect(SUBS,S.subs,'rows');
subsS = S(temp);
subsLS = sptensor(SUBS,subsL,[n1 n2 n3])+sptensor(temp,subsS,[n1 n2 n3]);
Sig_t = cp_als(subsLS,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Sig_t = cp_als(T,1);
Sig_t = Sig_t.lambda(1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D_t = subsLS-L_t;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%D_t = T-L_t;
thresh = thresh_const*Sig_t/sqrt(n1*n2*n3);
idx = unique([find(D_t > thresh); idx],'rows');
idx = unique([find(D_t < -thresh); idx],'rows');
S_t = sptensor([1 1 1],0,[n1 n2 n3]);
S_t(idx) = D_t(idx); % initial thresholding
if max(idx(:))==0
    idx = [];
end
while frob_err(t)/norm(subsLS)>=EPS && t<MAX_ITER % convergence check
    if ~mod(t, 1) % check progress
        fprintf('Iter no. %d\n', t);
    end
    t = t+1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    SUBS = samplepatterntensor(n1,r_hat);
    subsL = values(SUBS,L,true_r);
    temp = intersect(SUBS,S.subs,'rows');
    subsS = S(temp);
    temp2 = intersect(SUBS,S_t.subs,'rows');
    subsS2 = S_t(temp2);
    subsLSSt = sptensor(SUBS,subsL,[n1 n2 n3])+sptensor(temp,subsS,[n1 n2 n3])-sptensor(temp2,subsS2,[n1 n2 n3]);
    L_t = cp_als(subsLSSt, r_hat, 'printitn', 0);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    %L_t = cp_als(T-S_t, r_hat, 'printitn', 0);
    %sig_min = L_t.lambda(r_hat);
    %L_t = tensor(L_t);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    SUBS = samplepatterntensor(n1,r_hat);
    subsL = values(SUBS,L,true_r);
    temp = intersect(SUBS,S.subs,'rows');
    subsS = S(temp);
    subsLt = values(SUBS,L_t,r_hat);
    subsLSLt = sptensor(SUBS,subsL,[n1 n2 n3])+sptensor(temp,subsS,[n1 n2 n3])-sptensor(SUBS,subsLt,[n1 n2 n3]);
    D_t  = subsLSLt;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %D_t = double(T-L_t);
    thresh = (thresh_const/sqrt(n1*n2*n3));%*sig_min;%*w(r_hat, r_hat);
    idx = unique([find(D_t > thresh); idx],'rows');
    idx = unique([find(D_t < -thresh); idx],'rows');
    S_t(idx) = D_t(idx);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    SUBS = samplepatterntensor(n1,r_hat);
    subsL = values(SUBS,L,true_r);
    subsLt = values(SUBS,L_t,r_hat);
    subsLLt = sptensor(SUBS,subsL,[n1 n2 n3])-sptensor(SUBS,subsLt,[n1 n2 n3]);
    frob_err(t) = norm(subsLLt);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %frob_err(t) = norm(L-(L_t));
    if ((frob_err(t-1)-frob_err(t))/frob_err(t-1) <= TOL) && r_hat<true_r
        r_hat = r_hat+1; % use this for incrementally updating rank by 1
%         sig_t = lansvd(M-S_t, true_r, 'L'); % svd function from propack
%         ratio_sig = sig_t(r_hat+1:end)./[sig_t(r_hat+2:end); sig_t(end)];
%         [~, mx_idx] = max(ratio_sig);
%         r_hat = r_hat+mx_idx; % update rank for the next stage
    elseif ((frob_err(t-1)-frob_err(t))/frob_err(t-1) <= TOL) && r_hat==true_r
        thresh_const = thresh_const*thresh_red; % tune threshold
    end
%     if ((frob_err(t-1)-frob_err(t))/frob_err(t-1) <= TOL) && r_hat<=1.5*min([n1,n2,n3])
%         r_hat = r_hat+1;
%     elseif((frob_err(t-1)-frob_err(t))/frob_err(t-1) <= TOL) && r_hat==1.5*min([n1,n2,n3])%true_r
%         thresh_fact = thresh_fact*thresh_mult;
%     end
end
%S_t = double(S_t);
S_t(find(-EPS_S < S_t & S_t< EPS_S)) = 0;
%S_t = tensor(S_t);
iters = length(frob_err)-1;
end
