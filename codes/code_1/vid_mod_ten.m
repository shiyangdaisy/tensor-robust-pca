tic;
close all;
clear;
clc;
j = 1;
for i = 761:786%963
	T(:, :, j) = im2double(rgb2gray(imread(['curtain/Curtain23' num2str(i) '.bmp'])));
    j = j+1;
end

n1 = size(T, 1);
n2 = size(T, 2);
n3 = size(T, 3);

thresh_rate = 1-4e-1;
EPS = 1e-6; % convergence threshold
EPS_S = 1e-3; % convergence threshold
MAX_ITER = 20;
r_hat = 1;
T = tensor(T);

tic;
[T_t, L_t, S_t, iter_am, frob_err_am] = ls_alt_min_ten_thresh(T, r_hat, EPS, MAX_ITER, EPS_S);
time_alt_min = toc

figure;
semilogy(frob_err_am, 'b', 'LineWidth', 2, 'Marker', 'o'); 
hold;
grid;
S_t = double(S_t);
figure; imshow(S_t(:,:,15));
