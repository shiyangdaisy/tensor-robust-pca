tic;
close all;
clear;
clc;
M = im2double(rgb2gray(imread(['sub/' num2str(1) '/S3-T7-A.00001.jpeg'])));
numel_M = numel(M);
num_im = 9;
T = zeros(numel_M,num_im,4);
for i = 1:4
    for j = 1:num_im
        if j<=9
            M = im2double(rgb2gray(imread(['sub/' num2str(i) '/S3-T7-A.0000' num2str(j) '.jpeg'])));
            M = reshape(M,numel_M,1);
            T(:,j,i) = M;
        elseif j>=9 && j<=99
            M = im2double(rgb2gray(imread(['sub/' num2str(i) '/S3-T7-A.000' num2str(j) '.jpeg'])));
            M = reshape(M,numel_M,1);
            T(:,j,i) = M;
%            T(:, :, j) = im2double(rgb2gray(imread(['sub/' num2str(i) '/S3-T7-A.000' j '.jpeg'])));
        end
    end
end

n1 = size(T, 1);
n2 = size(T, 2);
n3 = size(T, 3);

thresh_rate = 1-4e-1;
EPS = 1e-6; % convergence threshold
EPS_S = 1e-3; % convergence threshold
MAX_ITER = 51;
r = 1;
r_hat = r;
T = tensor(T);

tic;
%[T_t, L_t, S_t, iter_am, frob_err_am] = ls_alt_min_ten_thresh(T, r_hat, EPS, MAX_ITER, EPS_S);
[T_t,L_t,S_t,iter_am,frob_err_am] = ls_alt_min_ten(T,r,r_hat,EPS,MAX_ITER,EPS_S);
time_alt_min = toc

%figure;
%semilogy(frob_err_am, 'b', 'LineWidth', 2, 'Marker', 'o'); 
%hold;
%grid;
S_t = double(S_t);
L_t = double(L_t);
%%figure; imshow(S_t(:,:,15));
img1 = im2double(rgb2gray(imread(['sub/' num2str(1) '/S3-T7-A.00001.jpeg'])));
figure; imshow(reshape(S_t(:,5,1),size(img1,1),size(img1,2)));
figure; imshow(reshape(L_t(:,5,1),size(img1,1),size(img1,2)));
save(['real_' 'na' num2str(n1) 'nb' num2str(n2) 'nc' num2str(n3) 'r' num2str(r) 'EPS' num2str(EPS) '.mat']);
%exit;
