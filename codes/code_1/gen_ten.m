close all;
clear;
clc;

seed = RandStream('mcg16807', 'Seed', 2);
RandStream.setDefaultStream(seed);

n1 = 100; % dimension
n2 = 100;
n3 = 100;

r = 5 % rank
incoh = 1 % incoherence (atleast 1)
tune_lambda = 0;
randn_mat = zeros(n1, n2, n3); % random iid gaussian submatrix for incoherence
z_n1 = floor(n1/incoh^2);
z_n2 = floor(n2/incoh^2);
z_n3 = floor(n3/incoh^2);
randn_ten(1:z_n1, 1:z_n2, 1:z_n3) = randn(z_n1, z_n2, z_n3);
L = cp_als(tensor(randn_ten), r, 'printitn', 0);
%[U, Sig, V] = svds(randn_mat, r);
%fact = (1-(1/(m*n)))/(r-1);
%vari = 0.05;
%for i = 1:r-1
%    mean = 1-(i-1)*fact;
%    Sig(i, i) = 1;%mean*(1 + vari*mean*randn);
%end
%Sig(r, r) = 1;%/(m*n);
L.lambda = ones(r,1);
L = tensor(L);

d = r; % degree
p = d/sqrt(n1*n2*n3); % bernoulli probability parameter
S = zeros(n1, n2, n3);
for i = 1:n1 % bernoulli model for true sparse part
    for j = 1:n2
    for k = 1:n3
        if rand < 0.5
            sgn = 1;
        else
            sgn = -1;
        end
        if rand < d/sqrt(n1*n2*n3) % do S_max/n
            S(i, j, k) = r*(0.5+rand/2)/sqrt(n1*n2*n3);
        end
    end
    end
end
S = tensor(S);
T = L+S; % true low rank+sparse matrix

r_hat = r;
EPS = 1e-8; % convergence threshold
EPS_S = 1e-3;
MAX_ITER = 50;
%lambda = 1/sqrt(m);

tic;
[T_t, L_t, S_t, iter_am, frob_err_am] = ls_alt_min_ten(T, r_hat, EPS, MAX_ITER, EPS_S);
time_alt_min = toc
% 
% if tune_lambda == 1
%     l = 1;
%     npts = 8;
%     for lambda = linspace(1/sqrt(m), 4/sqrt(m), npts)
%         tic;
%         [cell_A_hat{l}, cell_E_hat{l}, cell_iter_alm{l}, cell_frob_err_alm{l}] = inexact_alm_rpca(M, lambda, EPS, MAX_ITER);
%         time_tune_alm(l) = toc
%         l = l+1;
%     end
%     
%     for l = 1:length(cell_iter_alm)
%         arr_err_alm(l) = norm(cell_A_hat{l}-L, 'fro') / norm(L, 'fro');
%         [min_err_L min_err_L_idx] = min(arr_err_alm);
%     end
%     A_hat = cell_A_hat{min_err_L_idx};
%     E_hat = cell_E_hat{min_err_L_idx};
%     iter_alm = cell_iter_alm{l};
%     frob_err_alm = cell_frob_err_alm{l};
% else
%     tic;
%     [A_hat, E_hat, iter_alm, frob_err_alm] = inexact_alm_rpca(M, lambda, EPS, MAX_ITER);
%     %[A_hat, E_hat, iter_alm, frob_err_alm] = inexact_alm_rpca(M, lambda(min_err_idx), EPS, MAX_ITER);
%     time_alm = toc
% end
% 
rel_frob_err_am = frob_err_am/norm(T);
% rel_frob_err_alm = frob_err_alm/norm(M, 'fro');
% 
% err_am = norm(L_t-L, 'fro') / norm(L, 'fro')
% err_alm = norm(A_hat-L, 'fro') / norm(L, 'fro')
% rank_L_t = rank(L_t)
% rank_A_hat = rank(A_hat)
% sparsity_S_t = nnz(S_t)
% sparsity_E_hat = nnz(E_hat)
% sparsity_S = nnz(S)
% 
figure;
semilogy(1:iter_am, rel_frob_err_am(2:end), 'b', 'LineWidth', 2, 'Marker', 'o');
hold;
grid;
% semilogy(1:iter_alm, rel_frob_err_alm(2:end), 'r', 'LineWidth', 2, 'Marker', 'o');
% %title('Error');
% xlabel('Iterations');
% ylabel('Error');
% legend('AM', 'ALM');
% 
% save(['dat5000_tune_mu' num2str(incoh) '.mat']);
% %savefig(f, ['fig2000_tune_mu' num2str(incoh)]);
