close all;

f1 = figure;
set(gca,'fontsize',25);
%grid;
hold;
box;
eb1 = errorbar(15:5:40,mean(err_d,2),std(err_d,0,2),'b','LineWidth',3,'Marker', 'o');
%eb1 = errorbar(5:5:25,mean(time_d,2),std(time_d,0,2),'b','LineWidth',3,'Marker', 'o');
%eb2 = errorbar(400:100:800,mean(err_d(:,2,:),3),std(err_d(:,2,:),0,3),'r','LineWidth',3,'Marker', 'o');
%eb3 = errorbar(400:100:800,mean(err_d(:,3,:),3),std(err_d(:,3,:),0,3),'m','LineWidth',3);
set(get(eb1,'Parent'), 'YScale', 'log');
%set(get(eb2,'Parent'), 'YScale', 'log');
%set(get(eb3,'Parent'), 'YScale', 'log');
axis tight;
legend('NcRPCA','location','NorthWest');
legend('boxoff');
xlabel('n \alpha');
ylabel('Error');
%ylabel('Time');
title('n = 100, r = 4, \mu = 1');

saveTightFigure(f1, 'ten_err_d_2');
%%
f2 = figure;
set(gca,'fontsize',25);
%grid;
hold;
box;
eb1 = errorbar(15:5:40,mean(time_d,2),std(time_d,0,2),'b','LineWidth',3,'Marker', 'o');
%eb2 = errorbar(400:100:800,mean(time_d(:,2,:),3),std(time_d(:,2,:),0,3),'r','LineWidth',3,'Marker', 'o');
%eb3 = errorbar(400:100:800,mean(time_d(:,3,:),3),std(time_d(:,3,:),0,3),'m','LineWidth',3);
set(get(eb1,'Parent'), 'YScale', 'log');
%set(get(eb2,'Parent'), 'YScale', 'log');
%set(get(eb3,'Parent'), 'YScale', 'log');
axis tight;
legend('NcRPCA','location','NorthWest');
legend('boxoff');
xlabel('n \alpha');
ylabel('Time(s)');
title('n = 100, r = 4, \mu = 1');

saveTightFigure(f2, 'ten_time_d_2');

% f3 = figure;
% set(gca,'fontsize',25);
% %grid;
% hold;
% box;
% p = plot(rank_iters,'b','LineWidth',3,'Marker', 'o');
% set(get(p,'Parent'), 'YScale', 'log');
% axis tight;
% legend('IALM','location','NorthWest');
% legend('boxoff');
% xlabel('Iterations');
% ylabel('Rank');
% title('n = 2000, r = 5, \mu = 1');
% 
% saveTightFigure(f3, 'rank_d_2');
% 
% f4 = figure;
% set(gca,'fontsize',25);
% %grid;
% hold;
% box;
% mr = errorbar(400:100:800,mean(max_rank_d,2),std(max_rank_d,0,2),'b','LineWidth',3,'Marker','o');
% %p = plot(mean(max_rank_,'b','LineWidth',3,'Marker', 'o');
% %set(get(mr,'Parent'), 'YScale', 'log');
% axis tight;
% legend('IALM','location','NorthWest');
% legend('boxoff');
% xlabel('n \alpha');
% ylabel('Max. Rank');
% title('n = 2000, r = 5, \mu = 1');
% 
% saveTightFigure(f4, 'max_rank_d_2');
