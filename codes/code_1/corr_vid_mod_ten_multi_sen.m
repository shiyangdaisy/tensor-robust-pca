tic;
close all;
clear;
clc;
M = im2double(rgb2gray(imread(['sub/' num2str(5) '/Curtain23761.bmp'])));
%im_range = 1:end;
%M = M(im_range,im_range);
numel_M = numel(M);
num_im = 20;
%T = zeros(numel_M,numel_M,num_im);
aaa = zeros(numel_M,num_im);
for i = 5:5
    t = 1;
    for j = 761:780%1:num_im
        if j<=9
            M = im2double(rgb2gray(imread(['sub/' num2str(i) '/Curtain23' num2str(j) '.bmp'])));
            %M = M(im_range,im_range);
            M = reshape(M,numel_M,1);
            aaa(:,t) = M;
        else%if j>=9 && j<=99
            M = im2double(rgb2gray(imread(['sub/' num2str(i) '/Curtain23' num2str(j) '.bmp'])));
            %M = M(im_range,im_range);
            M = reshape(M,numel_M,1);
            aaa(:,t) = M;
            %aaa(:,:,t) = M*M';
%            T(:, :, j) = im2double(rgb2gray(imread(['sub/' num2str(i) '/S3-T7-A.000' j '.jpeg'])));
        end
        t = t+1;
    end
end

% n1 = size(T, 1);
% n2 = size(T, 2);
% n3 = size(T, 3);

thresh_rate = 1-4e-1;
EPS = 1e-6; % convergence threshold
EPS_S = 1e-3; % convergence threshold
MAX_ITER = 51;
r = num_im;
r_hat = 1;
%T = tensor(T);
T = ktensor(ones(num_im,1),aaa,aaa,diag(ones(num_im,1)));
ttt = cp_als(T,2);
%T = ktensor(ones(num_im,1),aaa,aaa,[diag(ones(num_im,1)); zeros(numel_M-num_im,num_im)]);
%%
disp('starting cp');
tic;
%[T_t, L_t, S_t, iter_am, frob_err_am] = ls_alt_min_ten_thresh(T, r_hat, EPS, MAX_ITER, EPS_S);
%[T_t,L_t,S_t,iter_am,frob_err_am] = ls_alt_min_ten_ktensor(T,r,r_hat,EPS,MAX_ITER,EPS_S);
time_alt_min = toc

%figure;
%semilogy(frob_err_am, 'b', 'LineWidth', 2, 'Marker', 'o'); 
%hold;
%grid;
S_t = double(S_t);
L_t = double(L_t);
%%figure; imshow(S_t(:,:,15));
%img1 = im2double(rgb2gray(imread(['sub/' num2str(1) '/S3-T7-A.00001.jpeg'])));
img1 = im2double(rgb2gray(imread(['sub/' num2str(5) '/Curtain23761.bmp'])));
figure; imshow(reshape(S_t(:,5,1),size(img1(im_range,im_range),1),size(img1(im_range,im_range),2)));
figure; imshow(reshape(L_t(:,5,1),size(img1(im_range,im_range),1),size(img1(im_range,im_range),2)));
%save(['real_' 'na' num2str(n1) 'nb' num2str(n2) 'nc' num2str(n3) 'r' num2str(r) 'EPS' num2str(EPS) '.mat']);
%exit;
