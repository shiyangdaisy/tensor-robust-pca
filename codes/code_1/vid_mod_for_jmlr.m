tic;
close all;
clear;
clc;
j = 1;
dat_path = '/Users/niranjan/Downloads/'; % './'
%M = zeros(19200,203);%2964); % restaurant
%M = zeros(81920,203);%2964); % shopping mall
M = zeros(20480,203);%2964); % curtain

for i = 761:963%786%963 % curtain
%for i = 1000:1102%786%963 % shopping mall
%for i = 1000:1102%786%963 % hotel/restaurant/bootstrap
%for i = 1000:3963 % read image frames
%	T(:, :, j) = im2double(rgb2gray(imread([dat_path 'Bootstrap/b0' num2str(i) '.bmp'])));
%	T(:, :, j) = im2double(rgb2gray(imread([dat_path 'ShoppingMall/ShoppingMall' num2str(i) '.bmp'])));
	T(:, :, j) = im2double(rgb2gray(imread([dat_path 'Curtain/Curtain21' num2str(i) '.bmp'])));
    M(:, j) = reshape(T(:,:,j), numel(T(:,:,j)), 1);
    j = j+1;
end
img1 = T(:,:,1);
imshow(T(:,:,70));
%%

m = size(T, 1);
n = size(T, 2);
p = size(T, 3);

thresh_rate = 1-4e-1;
EPS = 1e-6; % convergence threshold
EPS_S = 1e-3; % convergence threshold
MAX_ITER = 31;
r_hat = 1;
T = tensor(T);

tic;
%[T_t, L_t, S_t, iter_am, frob_err_am] = ls_alt_min_ten_thresh(T, r_hat, EPS, MAX_ITER, EPS_S);
[L_t_ten,S_t_ten,iter_ten,frob_err_ten] = ncrpca_ten_new(T,r_hat,EPS,MAX_ITER,EPS_S);
time_ten = toc
tic;
[L_t_mat,S_t_mat,iter_mat,frob_err_mat] = ncrpca(M,r_hat,EPS,MAX_ITER,EPS_S);
time_mat = toc

disp_frame_no = 100;
f_name = ['new_algo_Tvid_' 'm' num2str(m) 'n' num2str(n) 'r_hat' num2str(r_hat)];
imwrite(double(T(:,:,disp_frame_no)),[dat_path 'ncrpca_ten_output/' f_name '_orig.png']);
imwrite(double(L_t_ten(:,:,disp_frame_no)),[dat_path 'ncrpca_ten_output/' f_name '_L_t_ten.png']);
imwrite(double(S_t_ten(:,:,disp_frame_no)),[dat_path 'ncrpca_ten_output/' f_name '_S_t_ten.png']);
imwrite(reshape(L_t_mat(:,disp_frame_no),size(img1,1),size(img1,2)),[dat_path 'ncrpca_ten_output/' f_name '_L_t_mat.png']);
imwrite(reshape(S_t_mat(:,disp_frame_no),size(img1,1),size(img1,2)),[dat_path 'ncrpca_ten_output/' f_name '_S_t_mat.png']);
%tic;
%[UU, SS, VV] = lansvd(M, r_hat, 'L');
%tmp_mat = UU*SS*VV';
%time_pca = toc
%imwrite(reshape(tmp_mat(:,1), size(img1, 1), size(img1, 2)),[f_name '_pca.png']);
%tic;
%kt = cp_als(T,r_hat,'printitn',0);
%tmp_ten = tensor(kt);
%time_pca = toc
%imwrite(double(tmp_ten(:,:,disp_frame_no)),[f_name '_pca.png']);

% display the low rank components from the first frame with both methods
%figure; imshow(reshape(L_t(:,1), size(img1, 1), size(img1, 2)));
%figure; imshow(reshape(A_hat(:,1), size(img1, 1), size(img1, 2)));

nnz_S_t_ten = nnz(S_t_ten)
nnz_S_t_mat = nnz(S_t_mat)

save([dat_path 'ncrpca_ten_output/' 'new_algo_Tvid_' 'm' num2str(m) 'n' num2str(n) 'r_hat' num2str(r_hat) '.mat']);
%exit;
