clear;
for avg_runs = 1:5
i_d = 1
for d = 15:5:40
clearvars -except avg_runs i_d d time_d err_d;
clc;

n1 = 100;
n2 = 100;
n3 = 100;

r = 4;
n_init = r*5;
incoh = 1;
randn_mat = zeros(n1,n2,n3);

z_n1 = floor(n1/incoh^2);
rp = randperm(n1);
y1(rp(1:z_n1), :) = randn(z_n1, r);
z_n2 = floor(n2/incoh^2);
rp = randperm(n2);
y2(rp(1:z_n2), :) = randn(z_n2, r);
z_n3 = floor(n3/incoh^2);
rp = randperm(n3);
y3(rp(1:z_n3), :) = randn(z_n3, r);

y1 = y1./repmat(sqrt(sum(y1.*y1,1)),n1,1);
y2 = y2./repmat(sqrt(sum(y2.*y2,1)),n2,1);
y3 = y3./repmat(sqrt(sum(y3.*y3,1)),n3,1);

y2 = y1;
y3 = y1;

%lam_vec = sort(0.9+(rand(r,1)/10),'descend');
lam_vec = ones(r,1);
condn_no = lam_vec(1)/lam_vec(end);
L = tensor(ktensor(lam_vec,y1,y2,y3));

%d = r;
p = d/((n1*n2*n3)^(1/3));
S = rand(n1,n2,n3);
s_zero_idx = find(S>p);
s_nonzero_idx = find(S<=p);
S(s_zero_idx) = 0;
S(s_nonzero_idx) = (p*r/d)*(rand(length(s_nonzero_idx),1)/2+.5).*sign(randn(length(s_nonzero_idx),1));
S = tensor(S);

T = L+S;

r_hat = r;
EPS = 1e-3;
EPS_S = 1e-3;
MAX_ITER = 51;
TOL = 1e-1;

tic;
%[T_t,L_t,S_t,iter_am,frob_err_am] = am_ten_vary_d(T,n_init,r_hat,EPS,MAX_ITER,EPS_S,nnz(S),L,S);
%[T_t,L_t,S_t,iter_am,frob_err_am] = ls_alt_min_ten(T,r,r_hat,EPS,MAX_ITER,EPS_S);
[L_t,S_t,iter_am,frob_err_am] = ncrpca_ten_new(T,r,EPS,MAX_ITER,EPS_S,incoh,TOL);
%[T_t,L_t,S_t,iter_am,frob_err_am] = ls_alt_min_ten_st(T,r,r_hat,EPS,MAX_ITER,EPS_S);
%[w,a,b,c] = pow_ten_vary_d(T,n_init,r_hat,y1,y2,y3);
%norm(T-tensor(ktensor(w,a,b,c)))
%[T_t,L_t,S_t,iters,frob_err] = am_ten(T,n_init,r_hat,EPS,MAX_ITER,EPS_S,y1,y2,y3,S);
time_am = toc
err_am = norm(L-L_t)/norm(L)

time_d(i_d,avg_runs) = time_am;
err_d(i_d,avg_runs) = err_am;
save(['NEW_ten_avg_run' num2str(avg_runs) 'na' num2str(n1) 'nb' num2str(n2) 'nc' num2str(n3) 'r' num2str(r) 'incoh' num2str(incoh) 'd' num2str(d) 'cno' num2str(condn_no) 'EPS' num2str(EPS) '.mat']);
i_d = i_d+1;
end
end

save('time_d.mat','time_d');
save('err_d.mat','err_d');
