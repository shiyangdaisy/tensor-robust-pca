% This matlab script is used for comparing NcRPCA and convex RPCA (using
% IALM) on the foreground-background separation task. The datasets can be
% obtained at http://perception.i2r.a-star.edu.sg/bk_model/bk_index.html

close all;
clear;
clc;
j = 1;
M = zeros(20480,2964);
for i = 1000:3963 % read image frames
    img1 = im2double(rgb2gray(imread(['datasets/curtain/Curtain2' num2str(i) '.bmp'])));
    M(:, j) = reshape(img1, numel(img1), 1);
    j = j+1
end
[m n] = size(M);

EPS = 1e-3;
EPS_S = 1e-3;
MAX_ITER = 25;
r_hat = 10;

disp('starting low-rank+sparse matrix decomposition');
incoh = 1;
lambda = incoh/sqrt(m);
tic;
[L_t, S_t, iter_ncrpca, frob_err_ncrpca] = ncrpca(M, r_hat, EPS, MAX_ITER, EPS_S, incoh);
time_ncrpca = toc
tic;
[A_hat, E_hat, iter_alm, frob_err_alm] = inexact_alm_rpca(M, lambda, EPS, MAX_ITER);
time_alm = toc

f_name = ['new_algo_Tvid_' 'm' num2str(m) 'n' num2str(n) 'r_hat' num2str(r_hat)];
f1 = figure; imshow(reshape(M(:,1774), size(img1, 1), size(img1, 2)));
saveTightFigure(f1, [f_name '_orig']);
f2 = figure; imshow(reshape(L_t(:,1774), size(img1, 1), size(img1, 2)));
saveTightFigure(f2, [f_name '_L_t']);
f3 = figure; imshow(reshape(S_t(:,1774), size(img1, 1), size(img1, 2)));
saveTightFigure(f3, [f_name '_S_t']);
f4 = figure; imshow(reshape(A_hat(:,1774), size(img1, 1), size(img1, 2)));
saveTightFigure(f4, [f_name '_A_hat']);
f5 = figure; imshow(reshape(E_hat(:,1774), size(img1, 1), size(img1, 2)));
saveTightFigure(f5, [f_name '_E_hat']);
tic;
[UU, SS, VV] = lansvd(M, r_hat, 'L');
tmp_mat = UU*SS*VV';
time_pca = toc
f6 = figure; imshow(reshape(tmp_mat(:,1774), size(img1, 1), size(img1, 2)));
saveTightFigure(f6, [f_name '_pca']);

% display the low rank components from the first frame with both methods
%figure; imshow(reshape(L_t(:,1), size(img1, 1), size(img1, 2)));
%figure; imshow(reshape(A_hat(:,1), size(img1, 1), size(img1, 2)));

save(['new_algo_Tvid_' 'm' num2str(m) 'n' num2str(n) 'r_hat' num2str(r_hat) '.mat']);
exit;
