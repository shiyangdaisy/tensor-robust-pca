clear; clc;
%s = RandStream('mcg16807','Seed',0)
%RandStream.setDefaultStream(s)
X1 = 2*normc(randn(10000,1000));
X2 = 10*normc(randn(10000,1000));
X3 = 100*normc(randn(10000,1000));
[w,A,B,C] = cp_als_sample(X1,X2,X3,2);
t = cp_als(ktensor(ones(1000,1),X1,X2,X3), 2);
