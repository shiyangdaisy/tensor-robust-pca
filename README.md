# README #
Given an input tensor T = L + S, we aim to recover both L and S, where L is a
low rank tensor and S is a sparse tensor.

* This repository contains the codes for non-convex tensor robust principle component analysis.
* [Reference paper](http://arxiv.org/abs/1510.04747)

### Set Up and Descriptions###
* Before running the program, please download PROPACK: software for large and sparse SVD calculations
* codes/sampled_varymu/newsparsedS_varymu_whiten.m 
  
     Aims to compare the performace over three different methods varying the incoherence paramerter: mu. The three different methods are:tensor SSHOPM(not using whiten matrix);tensor SSHOPM(using whiten matrix);matrix AltProj

     Similarly you can vary other parameters such as sparse parameter d, tensor size n, and so on. 

* Some key algorithm functions:
  
     ncrpca_ten_new_whiten.m: tensor SSHOPM(using whiten matrix);
 
     ncrpca_ten_new.m: tensor SSHOPM(not using whiten matrix);
 
     ncrpca.m: matrix AltProj;



### Contact Information ###

* Yang Shi(shiy4@uci.edu)
* U.N. Niranjan(un.niranjan@uci.edu)